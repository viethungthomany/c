package exercise.lesson1;

import java.util.Scanner;

public class Lesson1 {
    private final Scanner scanner;

    private int[][] array;

    public Lesson1(Scanner sc){
        this.scanner = sc;
    }

    public void run(){  // Phương thức thực thi bài 1, chọn đề mục và chạy đề mục nếu chọn sai đề mục sẽ cho người dùng nhập lai
        int choseInt = 0;
        boolean isLoop = true;
        String notifyMessage = "";
        do{
            chooseJob();
            System.out.print(notifyMessage); // Là thông báo sau khi thực hiện xong công việc
            System.out.print("--> Mời bạn chọn công việc cần thực hiện theo số đề mục : ");
            String choseStr = scanner.next();
            boolean isWrong = false;
            if (isInteger(choseStr)){
                choseInt = Integer.parseInt(choseStr);
                if (choseInt < 0 || choseInt > 10){
                    chooseJob();
                    System.out.println("--> Bạn nhập sai số đề mục");
                    isWrong = true;
                }
            }else{
                chooseJob();
                System.out.println("--> Bạn nhập sai định dạng");
                isWrong = true;
            }
            if (isWrong) continue;

            switch (choseInt){
                case 1: notifyMessage = arrayQtyInput();
                    break;
                case 2: notifyMessage = arrayInput();
                    break;
                case 3: notifyMessage = printArray();
                    break;
                case 4: notifyMessage = getMaxMin();
                    break;
                case 5: notifyMessage = sortArrayDesc();
                    break;
                case 6: notifyMessage = getSumEvenNumber();
                    break;
                case 7: notifyMessage = getAverageDivisible3();
                    break;
                case 8: notifyMessage = getSumMax();
                    break;
                case 9: notifyMessage = symmetryCheck();
                    break;
                case 10: notifyMessage = arrayReverse();
                    break;
                case 0: isLoop = false;
                    System.out.println("--> Kết thúc chương trình");
                    break;
                default:
                    break;
            }
        }while (isLoop);
    }

    private void chooseJob(){
        System.out.println("<-------<<  Chương trình thao tác với mảng hai chiều  >>------->");
        System.out.println("------------------------------MENU------------------------------");
        System.out.println("** 1. Nhập số hàng và cột của mảng                            **");
        System.out.println("** 2. Nhập mảng                                               **");
        System.out.println("** 3. Xuất mảng                                               **");
        System.out.println("** 4. Lấy ra phẩn tử có giá trị lớn nhất và nhỏ nhất          **");
        System.out.println("** 5. Sắp xếp mảng theo thứ tự giảm dần                       **");
        System.out.println("** 6. Lấy tổng các số chẵn trong mảng                         **");
        System.out.println("** 7. Lấy trung bình cộng của các phần tử chia hết cho 3      **");
        System.out.println("** 8. Lấy ra tổng số phần tử có giá trị lớn nhất              **");
        System.out.println("** 9. Kiểm tra tính đối xứng của mảng                         **");
        System.out.println("** 10. Đảo ngược mảng                                         **");
        System.out.println("** 0. Thoát chương trình                                      **");
        System.out.println("----------------------------------------------------------------");
    }

    private boolean isInteger(String str){
        try {
            Integer.parseInt(str);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    private String arrayQtyInput(){
        int rowQty = 0;
        int colQty = 0;
        boolean rowQtyCorrect = false;
        boolean colQtyCorrect = false;
        do{
            if (!rowQtyCorrect){
                System.out.print("- 1: Mời bạn nhập số hàng: ");
                String rowQtyStr = scanner.next();
                boolean isWrong = false;
                if (isInteger(rowQtyStr)){
                    rowQty = Integer.parseInt(rowQtyStr);
                    if (rowQty < 0){
                        System.out.print("- 1: Không thể nhập số hàng bé hơn 0\n\n");
                        isWrong = true;
                    }
                }else{
                    System.out.print("- 1: Bạn nhập sai định dạng\n\n");
                    isWrong = true;
                }
                if (isWrong) continue;
                rowQtyCorrect = true;
                rowQty = Integer.parseInt(rowQtyStr);
            }
            System.out.print("- 1: Mời bạn nhập số cột: ");
            String colQtyStr = scanner.next();

            if (isInteger(colQtyStr)){
                colQty = Integer.parseInt(colQtyStr);
                if (colQty < 0){
                    System.out.print("- 1: Không thể nhập số cột bé hơn 0\n\n");
                }else{
                    colQtyCorrect = true;
                }
            }else{
                System.out.print("- 1: Bạn nhập sai định dạng\n\n");
            }
        }while (!rowQtyCorrect || !colQtyCorrect);

        array = new int[rowQty][colQty];
        return "--> Mảng của bạn có "+rowQty+" hàng"+" và "+colQty+" cột\n";
    }

    private String arrayInput(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        int rowQty = array.length;
        int colQty = array[0].length;
        System.out.println();
        for (int i = 0; i < rowQty; i++) {
            for (int j = 0; j < colQty; j++) {
                boolean isLoop = false;
                do {
                    System.out.print("- 1: Nhập hàng "+(i+1)+" Cột "+(j+1)+" : ");
                    String value = scanner.next();
                    if (isInteger(value)){
                        array[i][j] = Integer.parseInt(value);
                        isLoop = false;
                    }else{
                        System.out.println("- 1: Bạn nhập sai định dạng");
                        isLoop = true;
                    }
                }while (isLoop);
            }
            System.out.println();
        }
        return "--> Nhập phần tử cho mảng thành công\n";
    }

    private String printArray(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        StringBuilder notifyMessage = new StringBuilder("***** xuất mảng *****\n");
        for (int i = 0; i < array.length; i++) {
            if (i > 0) notifyMessage.append("\n");
            for (int j = 0; j < array[0].length; j++) {
                notifyMessage.append("-> Hàng ").append(i).append(" cột ").append(j).append(" : ").append(array[i][j]).append("\n");
            }
        }
        notifyMessage.append("********************\n");
        return notifyMessage.toString();
    }

    private String getMaxMin(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        int min = array[0][0];
        int max = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (array[i][j] < min) min = array[i][j];
                if (array[i][j] > max) max = array[i][j];
            }
        }
        return "--> Phần tử lớn nhất là: "+max+", phần tử nhỏ nhất là: "+min+"\n";
    }

    private String sortArrayDesc(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        StringBuilder notifyMessage = new StringBuilder("--> Mảng trước sắp xếp\n");
        notifyMessage.append(printArray());
        notifyMessage.append("\n--> Mảng sau khi sắp xếp\n");
        int length = array.length * array[0].length; //chiều dài khi toàn bộ phần tử theo 1 chiều
        int colSize = array[0].length;
        for (int i = 0; i < length - 1; i++) {
            boolean swap = false;
            for (int j = 0; j < length - i - 1; j++) {
                int x = j / colSize;
                int y = j % colSize;
                int x2 = (j+1) / colSize;
                int y2 = (j+1) % colSize;
                if (array[x][y] < array[x2][y2]){
                    int temp = array[x][y];
                    array[x][y] = array[x2][y2];
                    array[x2][y2] = temp;
                    swap = true;
                }
            }
            if (!swap) break;
        }
        notifyMessage.append(printArray());
        return notifyMessage.toString();
    }

    private String getSumEvenNumber(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        int sum = 0;
        for (int[] ints : array) {
            for (int j = 0; j < array[0].length; j++) {
                if (ints[j] % 2 == 0) sum += ints[j];
            }
        }
        return "--> Tổng các số chẵn trong mảng là: "+sum+"\n";
    }

    private String getAverageDivisible3(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        int sum = 0;
        int count = 0;
        for (int[] ints : array) {
            for (int j = 0; j < array[0].length; j++) {
                if (ints[j] % 3 == 0){
                    sum += ints[j];
                    count++;
                }
            }
        }
        int result = count == 0 ? 0 : sum / count;
        return "--> Trung bình cộng của các phần tử chia hết cho 3 là: "+result+"\n";
    }

    private String getSumMax(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        int sum = 0;
        int max = array[0][0];
        for (int[] ints : array) {
            for (int j = 0; j < array[0].length; j++) {
                if (ints[j] > max){
                    max = ints[max];
                    sum = 0;
                }
                if (ints[j] == max) sum+= max;
            }
        }
        return "--> Tổng số các phần tử có giá trị lớn nhất là: "+sum+"\n";
    }

    private String symmetryCheck(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        boolean check = true;
        int length = array.length * array[0].length;
        int mid = length / 2;
        int colSize = array[0].length;
        for (int i = 0; i < mid - 1; i++) {
            int oppIndex = length - i - 1; // vị trí ngược lại
            int x = i / colSize;
            int y = i % colSize;
            int x2 = oppIndex / colSize;
            int y2 = oppIndex % colSize;
            if (array[x][y] != array[x2][y2]){
                check = false;
                break;
            }
        }
        if (!check) return "--> Mảng này là mảng không đối xứng\n";

        return "--> Mảng này là mảng đối xứng\n";
    }

    private String arrayReverse(){
        if (array == null){
            return "--> Bạn chưa khởi tạo mảng hay thực hiện công việc 1\n";
        }
        int[][] temp = new int[array.length][array[0].length];
        int index = 0;
        int colSize = array[0].length;
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = colSize - 1; j >= 0; j--) {
                int x = index / colSize;
                int y = index % colSize;
                temp[x][y] = array[i][j];
                index++;
            }
        }
        return "--> Đảo ngược mảng thành công, đây là mảng sau khi đảo ngược:\n" + printArray();
    }
}
