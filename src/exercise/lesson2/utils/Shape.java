package exercise.lesson2.utils;

public interface Shape {
    double perimeter();
    double area();
}
