package exercise.lesson2.utils.impl;

import exercise.lesson2.utils.Shape;

public class Triangle implements Shape {
    private double base; // Độ dài cạnh đáy
    private double height; // Chiều cao tương ứng với đáy
    private double sideA; // Độ dài cạnh a
    private double sideB; // Độ dài cạnh b
    private double sideC; // Độ dài cạnh c

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    @Override
    public double perimeter() {
        return sideA + sideB + sideC;
    }

    @Override
    public double area() {
        return (base * height) / 2;
    }
}
