package exercise.lesson2.utils.impl;

import exercise.lesson2.utils.Shape;

public class Square implements Shape {
    private double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double perimeter() {
        return side * side;
    }

    @Override
    public double area() {
        return side * 4;
    }
}
