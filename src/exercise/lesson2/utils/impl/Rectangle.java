package exercise.lesson2.utils.impl;

import exercise.lesson2.utils.Shape;

public class Rectangle implements Shape {
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public double perimeter() {
        return length * width;
    }

    @Override
    public double area() {
        return (length + width) / 2;
    }
}
