package exercise.lesson2.utils.impl;

import exercise.lesson2.utils.Shape;

public class Circle implements Shape {
    private double radius = 0;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double area() {
        return Math.PI * Math.pow(radius, 2);
    }
}
