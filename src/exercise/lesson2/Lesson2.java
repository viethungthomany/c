package exercise.lesson2;

import exercise.lesson2.utils.impl.Square;

import java.util.Scanner;

public class Lesson2 {
    private final Scanner scanner;

    public Lesson2(Scanner scanner) {
        this.scanner = scanner;
    }


    public void run(){
        // Có thể làm menu tại đây nhưng vì em lười nên thôi :))
        Square square = new Square(5);
        square.perimeter();
        square.area();
    }
}
