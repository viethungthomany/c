package exercise.lesson3;

import exercise.lesson3.utils.TextFileUtils;

import java.io.IOException;
import java.util.Scanner;

public class Lesson3 {
    private final Scanner scanner;

    public Lesson3(Scanner scanner) {
        this.scanner = scanner;
    }

    public void run(){
        String filePath = "data.txt";
        System.out.println("--> Mời bạn nhập dữ liệu");
        String[] input = new String[5];
        StringBuilder text = new StringBuilder();
        for (String s : input) {
            s = scanner.nextLine();
            text.append(s).append("\n");
        }
        try {
            TextFileUtils.writeTextFile(filePath, text.toString());
            System.out.println("--> Đã ghi file thành công ra file data.txt");
            System.out.println("--> Đọc file: ");
            String fileContent = TextFileUtils.readTextFile(filePath);
            System.out.println(fileContent);
            TextFileUtils.renameTextFile(filePath, "newdata.txt");
        }catch (IOException e){
            System.out.println("--> Đã xảy ra lỗi khi đọc/ghi file: " + e.getMessage());
        }
    }
}
