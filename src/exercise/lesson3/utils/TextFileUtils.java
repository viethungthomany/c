package exercise.lesson3.utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;

public class TextFileUtils {
    public static String readTextFile(String filePath) throws IOException {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        }
        return content.toString();
    }

    public static void writeTextFile(String filePath, String content) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(content);
        }
    }

    public static void renameTextFile(String filePath, String newFileName) {
        File file = new File(filePath);
        String parentPath = file.getParent();
        String newFilePath = parentPath + File.separator + newFileName;

        File newFile = new File(newFilePath);
        if (file.renameTo(newFile)) {
            System.out.println("Đổi tên file thành công.");
        } else {
            System.out.println("Đổi tên file không thành công.");
        }
    }
}
