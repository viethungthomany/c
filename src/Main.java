import exercise.lesson1.Lesson1;
import exercise.lesson2.Lesson2;
import exercise.lesson3.Lesson3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Lesson1 lesson1 = new Lesson1(scanner);
        lesson1.run();
//        Lesson2 lesson2 = new Lesson2(scanner);
//        lesson2.run();
//        Lesson3 lesson3 = new Lesson3(scanner);
//        lesson3.run();
    }
}
